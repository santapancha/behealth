package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.SolicitudCompra;

@ManagedBean(name="solicitudCompraCTL")
@RequestScoped
public class SolicitudCompraCTL {
	private SolicitudCompra solicitudCompra;
	private Set<SolicitudCompra> solicitudCompraList;
	
	public SolicitudCompraCTL() {
		super();
	}
	
	public SolicitudCompraCTL(SolicitudCompra solicitudCompra, Set<SolicitudCompra> solicitudCompraList) {
		super();
		this.solicitudCompra = solicitudCompra;
		this.solicitudCompraList = solicitudCompraList;
	}
	
	public SolicitudCompra getSolicitudCompra() {
		return solicitudCompra;
	}
	
	public void setSolicitudCompra(SolicitudCompra solicitudCompra) {
		this.solicitudCompra = solicitudCompra;
	}
	
	public Set<SolicitudCompra> getSolicitudCompraList() {
		return solicitudCompraList;
	}
	
	public void setSolicitudCompraList(Set<SolicitudCompra> solicitudCompraList) {
		this.solicitudCompraList = solicitudCompraList;
	}
	
	public void crearSolicitudCompra() {
		
	}
	
	public void modificarSolicitudCompra() {
		
	}
	
	public void eliminarSolicitudCompra() {
		
	}
	
	public void leerUnaSolicitudCompra() {
		
	}
	
	public void leerTodasSolicitudCompras() {
		
	}
}