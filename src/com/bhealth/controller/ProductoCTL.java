package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.Producto;

@ManagedBean(name="productoCTL")
@RequestScoped
public class ProductoCTL {
	private Producto producto;
	private Set<Producto> productoList;
	
	public ProductoCTL() {
		super();
	}

	public ProductoCTL(Producto producto, Set<Producto> productoList) {
		super();
		this.producto = producto;
		this.productoList = productoList;
	}
	
	public Producto getProducto() {
		return producto;
	}
	
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	public Set<Producto> getProductoList() {
		return productoList;
	}
	
	public void setProductoList(Set<Producto> productoList) {
		this.productoList = productoList;
	}
	
	public void crearProducto() {
		
	}
	
	public void modificarProducto() {
		
	}
	
	public void eliminarProducto() {
		
	}
	
	public void leerUnProducto() {
		
	}
	
	public void leerTodosProductos() {
		
	}
}