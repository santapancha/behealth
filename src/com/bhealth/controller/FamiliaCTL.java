package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import com.bhealth.model.Familia;

@ManagedBean(name="familiaCTL")
@RequestScoped()
public class FamiliaCTL {
	private Familia familia;
	private Set<Familia> familiaSet;
	
	public FamiliaCTL() {
		super();
	}
	
	public FamiliaCTL(Familia familia, Set<Familia> familiaSet) {
		super();
		this.familia = familia;
		this.familiaSet = familiaSet;
	}
	
	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	public Set<Familia> getFamiliaSet() {
		return familiaSet;
	}

	public void Familia(Set<Familia> familiaSet) {
		this.familiaSet= familiaSet;
	}

	public void crearFamilia() {
		// Mandará a llamar al método del DAO que inserte la nueva familia
	}
	
	public void modificarFamilia() {
		// Mandará a llamar al método del DAO que modifique la familia
	}
	
	public void eliminarFamilia() {
		// Mandará a llamar al método del DAO que elimine la familia
	}
	
	public void leerUnaFamilia() {
		// Mandará a llamar al método del DAO que lea una familia
	}
	
	public void leerTodasFamilia() {
		// Mandará a llamar al método del DAO que lea  todas las familia
	}
}
