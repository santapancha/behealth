package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.Departamento;

@ManagedBean(name="departamentoCTL")
@RequestScoped()
public class DepartamentoCTL {
	private Departamento departamento;
	private Set<Departamento> departamentoSet;
	
	public DepartamentoCTL() {
		super();
	}
	
	public DepartamentoCTL(Departamento departamento, Set<Departamento> departamentoSet) {
		super();
		this.departamento = departamento;
		this.departamentoSet = departamentoSet;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Set<Departamento> getDepartamentoSet() {
		return departamentoSet;
	}

	public void setDepartamentoSet(Set<Departamento> departamentoSet) {
		this.departamentoSet = departamentoSet;
	}

	public void crearDepartamento() {
		// Mandará a llamar al método del DAO que inserte el nuevo departamento
	}
	
	public void modificarDepartamento() {
		// Mandará a llamar al método del DAO que modifique el departamento
	}
	
	public void eliminarDepartamento() {
		// Mandará a llamar al método del DAO que elimine el departamento
	}
	
	public void leerUnDepartamento() {
		// Mandará a llamar al método del DAO que lea un departamento
	}
	
	public void leerTodosDepartamento() {
		// Mandará a llamar al método del DAO que lea  todos los departamento
	}
}
