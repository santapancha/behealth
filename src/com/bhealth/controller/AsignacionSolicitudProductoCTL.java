package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import com.bhealth.model.AsignacionSolicitudProducto;

@ManagedBean(name="asignacionSolicitudProductoCTL")
@RequestScoped()
public class AsignacionSolicitudProductoCTL {
	private AsignacionSolicitudProducto aSolicitudProducto;
	private Set<AsignacionSolicitudProducto> aSolicitudProductoSet;
	
	public AsignacionSolicitudProductoCTL() {
		super();
	}
	
	public AsignacionSolicitudProductoCTL(AsignacionSolicitudProducto aSolicitudProducto, Set<AsignacionSolicitudProducto> aSolicitudProductoSet) { //Se elimino lo que estaba en <> y quedo el metodo Set aSolicitudProductoSet
		super();
		this.aSolicitudProducto = aSolicitudProducto;
		this.aSolicitudProductoSet = aSolicitudProductoSet;
	}
	
	public AsignacionSolicitudProducto getASolicitudProducto()
	{
		return aSolicitudProducto;
	}

	public void setASolicitudProducto(AsignacionSolicitudProducto aSolicitudProducto)
	{
		this.aSolicitudProducto = aSolicitudProducto;
	}

	public Set<AsignacionSolicitudProducto> getaSolicitudProductoSet() 
	{
		return aSolicitudProductoSet;
	}

	public void setASolicitudProductoSet(Set<AsignacionSolicitudProducto> aSolicitudProductoSet) 
	{
		this.aSolicitudProductoSet = aSolicitudProductoSet;
	}

	public void crearASolicitudProducto() 
	{
		// Mandar a llamar al m�todo del DAO que inserte la nueva Asignaci�n Solicitud
	}
	
	public void modificarASolicitudProducto()
	{
		// Mandar a llamar al m�todo del DAO que modifique la Asignaci�n Solicitud
	}
	
	public void eliminarASolicitudProducto() 
	{
		// Mandar a llamar al m�todo del DAO que elimine la Asignaci�n solicitud
	}
	
	public void leerUnaASolicitudProducto() 
	{
		// Mandar a llamar al m�todo del DAO que lea una Asignaci�n solicitud
	}
	
	public void leerTodosASolicitudProducto() 
	{
		// Mandar a llamar al m�todo del DAO que lea  todos las Asignaciones Solicitud
	}
}
