package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.StatusCompra;

@ManagedBean(name="statusCompraCTL")
@RequestScoped
public class StatusCompraCTL {
	private StatusCompra statusCompra;
	private Set<StatusCompra> statusCompraList;
	
	public StatusCompraCTL() {
		super();
	}
	
	public StatusCompraCTL(StatusCompra statusCompra, Set<StatusCompra> statusCompraList) {
		super();
		this.statusCompra = statusCompra;
		this.statusCompraList = statusCompraList;
	}
	
	public StatusCompra getStatusCompra() {
		return statusCompra;
	}
	
	public void setStatusCompra(StatusCompra statusCompra) {
		this.statusCompra = statusCompra;
	}
	
	public Set<StatusCompra> getStatusCompraList() {
		return statusCompraList;
	}
	
	public void setStatusCompraList(Set<StatusCompra> statusCompraList) {
		this.statusCompraList = statusCompraList;
	}
	
	public void crearStatusCompra() {
		
	}
	
	public void modificarStatusCompra() {
		
	}
	
	public void eliminarStatusCompra() {
		
	}
	
	public void leerUnaStatusCompra() {
		
	}
	
	public void leerTodasStatusCompras() {
		
	}
}
