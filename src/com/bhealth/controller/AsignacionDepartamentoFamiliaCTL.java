package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import com.bhealth.model.AsignacionDepartamentoFamilia;

@ManagedBean(name="asignacionDepartamentoFamiliaCTL")
@RequestScoped()
public class AsignacionDepartamentoFamiliaCTL {
	private AsignacionDepartamentoFamilia aDepartamentoFamilia;
	private Set<AsignacionDepartamentoFamilia> aDepartamentoFamiliaSet;
	
	public AsignacionDepartamentoFamiliaCTL() {
		super();
	}
	
	public AsignacionDepartamentoFamiliaCTL(AsignacionDepartamentoFamilia aDepartamentoFamilia, Set<AsignacionDepartamentoFamilia> aDepartamentoFamiliaSet) {
		super();
		this.aDepartamentoFamilia = aDepartamentoFamilia;
		this.aDepartamentoFamiliaSet = aDepartamentoFamiliaSet;
	}
	
	public AsignacionDepartamentoFamilia getADepartamentoFamilia()
	{
		return aDepartamentoFamilia;
	}

	public void setADepartamentoFamilia(AsignacionDepartamentoFamilia aDepartamentoFamilia)
	{
		this.aDepartamentoFamilia = aDepartamentoFamilia;
	}

	public Set<AsignacionDepartamentoFamilia> getADepartamentoFamiliaSet() 
	{
		return aDepartamentoFamiliaSet;
	}

	public void setADepartamentoFamiliaSet(Set<AsignacionDepartamentoFamilia> aDepartamentoFamiliaSet) 
	{
		this.aDepartamentoFamiliaSet = aDepartamentoFamiliaSet;
	}

	public void crearADepartamentoFamilia() 
	{
		// Mandar a llamar al m�todo del DAO que inserte la nueva Asignacion Departamento Familia
	}
	
	public void modificarADepartamentoFamilia()
	{
		// Mandar a llamar al m�todo del DAO que modifique las Asignaciones Departamentos Familia
	}
	
	public void eliminarADepartamentoFamilia() 
	{
		// Mandar a llamar al m�todo del DAO que elimine las Asignaciones Departamentos Familias
	}
	
	public void leerUnaADepartamentoFamilia() 
	{
		// Mandar a llamar al m�todo del DAO que lea una Asignacion Departamento Familia
	}
	
	public void leerTodasADepartamentoFamilia() 
	{
		// Mandar a llamar al m�todo del DAO que lea  todos las Asignaciones Departamentos Familias
	}
}
