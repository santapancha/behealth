package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.Unidad;

@ManagedBean(name="unidadCTL")
@RequestScoped
public class UnidadCTL {
	private Unidad unidad;
	private Set<Unidad> unidadSet;
	
	public UnidadCTL() {
		super();
	}

	public UnidadCTL(Unidad unidad, Set<Unidad> unidadSet) {
		super();
		this.unidad = unidad;
		this.unidadSet = unidadSet;
	}

	public Unidad getUnidad() {
		return unidad;
	}

	public void setUndiad(Unidad unidad) {
		this.unidad = unidad;
	}

	public Set<Unidad> getUnidadSet() {
		return unidadSet;
	}

	public void setUnidadSet(Set<Unidad> unidadSet) {
		this.unidadSet = unidadSet;
	}
	
	public void crearUnidad() {
		
	}
	
	public void modificarUnidad() {
		
	}
	
	public void eliminarUnidad() {
		
	}
	
	public void leerUnaUnidad() {
		
	}
	
	public void leerTodasUnidad() {
		
	}
}
