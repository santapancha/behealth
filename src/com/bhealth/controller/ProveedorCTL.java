package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.Proveedor;

@ManagedBean(name="proveedorCTL")
@RequestScoped
public class ProveedorCTL {
	private Proveedor proveedor;
	private Set<Proveedor> proveedorSet;
	
	public ProveedorCTL() {
		super();
}

	public ProveedorCTL(Proveedor proveedor, Set<Proveedor> proveedorSet) {
		super();
		this.proveedor = proveedor;
		this.proveedorSet = proveedorSet;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public Set<Proveedor> getProveedorSet() {
		return proveedorSet;
	}

	public void setProveedorSet(Set<Proveedor> proveedorSet) {
		this.proveedorSet = proveedorSet;
	}
	
	public void crearProveedor() {
		
	}
	
	public void modificarProveedor() {
		
	}
	
	public void eliminarProveedor() {
		
	}
	
	public void leerUnProveedor() {
		
	}
	
	public void leerTodosProveedor() {
		
	}
}