package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.Usuario;

@ManagedBean(name="usuarioCTL")
@RequestScoped
public class UsuarioCTL {
	private Usuario usuario;
	private Set<Usuario> usuarioSet;
	
	public UsuarioCTL() {
		super();
	}

	public UsuarioCTL(Usuario usuario, Set<Usuario> usuarioSet) {
		super();
		this.usuario = usuario;
		this.usuarioSet = usuarioSet;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Set<Usuario> getUsuarioSet() {
		return usuarioSet;
	}

	public void setUsuarioSet(Set<Usuario> usuarioSet) {
		this.usuarioSet = usuarioSet;
	}
	
	public void crearUsuario() {
		
	}
	
	public void modificarUsuario() {
		
	}
	
	public void eliminarUsuario() {
		
	}
	
	public void leerUnUsuario() {
		
	}
	
	public void leerTodosUsuario() {
		
	}
}