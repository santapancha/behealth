package com.bhealth.controller;

import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.bhealth.model.Compra;

@ManagedBean(name="compraCTL")
@RequestScoped
public class CompraCTL {
	private Compra compra;
	private Set<Compra> compraList;
	
	public CompraCTL() {
		super();
	}
	
	public CompraCTL(Compra compra, Set<Compra> compraList) {
		super();
		this.compra = compra;
		this.compraList = compraList;
	}

	public Compra getCompra() {
		return compra;
	}

	public void setCompra(Compra compra) {
		this.compra = compra;
	}

	public Set<Compra> getCompraList() {
		return compraList;
	}

	public void setCompraList(Set<Compra> compraList) {
		this.compraList = compraList;
	}
	
	public void crearCompra() {
		
	}
	
	public void modificarCompra() {
		
	}
	
	public void eliminarCompra() {
		
	}
	
	public void leerUnaCompra() {
		
	}
	
	public void leerTodasCompras() {
		
	}
}
