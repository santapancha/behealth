package com.bhealth.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bhealth.model.Unidad;

public class UnidadDAO {
	public static Connection conn;
	public static PreparedStatement ps;
	public static ResultSet rs;
	public static String url ="jdbc:mysql://localhost:3306/bhealth?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	public static String user = "root";
	public static String pass = "9850"; //Duda en la contrase�a
	
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.Driver");
			conn = DriverManager.getConnection(url, user, pass);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static ArrayList<Unidad> getTodasUnidadBD() {
		ArrayList<Unidad> unidadLista = new ArrayList<Unidad>();
		try {
			ps = getConnection().prepareStatement("SELECT * FROM unidades;");
			rs = ps.executeQuery();
			while(rs.next()) {
				Unidad unidad = new Unidad();
				unidad.setId(Integer.parseInt(rs.getString("id_unidades")));
				unidad.setNombre(rs.getString("nombre_unidadess"));
				unidadLista.add(unidad);
			}
			System.out.println("Número de filas: "+unidadLista.size());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return unidadLista;
	}
	
	public static Unidad leerUnaUnidadBD(Unidad unidad) {
		try {
			ps.getConnection().prepareStatement("SELECT * FROM unidades WHERE id_unidades = ?;");
			rs = ps.executeQuery();
			unidad.setNombre(rs.getString("nombre_unidades"));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return unidad;
	}
	
	public static String crearUnidadBD(Unidad unidad) {
		int numReg = 0;
		String navResult = "";
		try {
			ps.getConnection().prepareStatement("INSERT INTO unidades (nombre_unidades) VALUES(?);");
			ps.setString(2, unidad.getNombre());
			numReg = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(numReg != 0) {
			navResult = "unidadLista.xhtml?faces-redirect=true";
		} else {
			navResult = "unidadCrear.xhtml?faces-redirect=true";
		}
		return navResult;
	}
	
	public static String modificarUnidadBD(Unidad unidad) {
		int numReg = 0;
		String navResult = "";
		try {
			ps.getConnection().prepareStatement("UPDATE unidades SET nombre_unidades = ? WHERE id_unidades = ?;");
			ps.setString(2, unidad.getNombre());
			numReg = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(numReg != 0) {
			navResult = "unidadLista.xhtml?faces-redirect=true";
		} else {
			navResult = "unidadModificar.xhtml?faces-redirect=true";
		}
		return navResult;
	}
	
	public static String eliminarUnidadBD(Unidad unidad) {
		int numReg = 0;
		String navResult = "";
		try {
			ps.getConnection().prepareStatement("DELETE FROM unidades WHERE id_unidades = ?;");
			ps.setString(2, unidad.getNombre());
			numReg = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(numReg != 0) {
			navResult = "unidadLista.xhtml?faces-redirect=true";
		} else {
			navResult = "unidadLista.xhtml?faces-redirect=true";
		}
		return navResult;
	}
}