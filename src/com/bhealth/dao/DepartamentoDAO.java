package com.bhealth.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.bhealth.model.Departamento;

public class DepartamentoDAO {
	public static Connection conn;
	public static PreparedStatement ps;
	public static ResultSet rs;
	public static String url ="jdbc:mysql://localhost:3306/bhealth?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	public static String user = "root";
	public static String pass = "9850";
	
	public static Connection getConnection() {
		try {
			Class.forName("com.mysql.cj.Driver");
			conn = DriverManager.getConnection(url, user, pass);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static ArrayList<Departamento> getTodosDepartamentoBD() {
		ArrayList<Departamento> departamentoLista = new ArrayList<Departamento>();
		try {
			ps = getConnection().prepareStatement("SELECT * FROM departamentos;");
			rs = ps.executeQuery();
			while(rs.next()) {
				Departamento departamento = new Departamento();
				departamento.setId(Integer.parseInt(rs.getString("id_departamentos")));
				departamento.setNombre(rs.getString("nombre_departamentos"));
				departamentoLista.add(departamento);
			}
			System.out.println("Número de filas: "+departamentoLista.size());
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return departamentoLista;
	}
	
	public static Departamento leerUnoDepartamentoBD(Departamento departamento) {
		try {
			ps.getConnection().prepareStatement("SELECT * FROM departamentos WHERE id_departamentos = ?;");
			rs = ps.executeQuery();
			departamento.setNombre(rs.getString("nombre_departamentos"));
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return departamento;
	}
	
	public static String crearDepartamentoBD(Departamento departamento) {
		int numReg = 0;
		String navResult = "";
		try {
			ps.getConnection().prepareStatement("INSERT INTO departamentos (nombre_departamentos) VALUES(?);");
			ps.setString(2, departamento.getNombre());
			numReg = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(numReg != 0) {
			navResult = "departamentoLista.xhtml?faces-redirect=true";
		} else {
			navResult = "departamentoCrear.xhtml?faces-redirect=true";
		}
		return navResult;
	}
	
	public static String modificarDepartamentoBD(Departamento departamento) {
		int numReg = 0;
		String navResult = "";
		try {
			ps.getConnection().prepareStatement("UPDATE departamentos SET nombre_departamentos = ? WHERE id_departamentos = ?;");
			ps.setString(2, departamento.getNombre());
			numReg = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(numReg != 0) {
			navResult = "departamentoLista.xhtml?faces-redirect=true";
		} else {
			navResult = "departamentoModificar.xhtml?faces-redirect=true";
		}
		return navResult;
	}
	
	public static String eliminarDepartamentoBD(Departamento departamento) {
		int numReg = 0;
		String navResult = "";
		try {
			ps.getConnection().prepareStatement("DELETE FROM departamentos WHERE id_departamentos = ?;");
			ps.setString(2, departamento.getNombre());
			numReg = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if(numReg != 0) {
			navResult = "departamentoLista.xhtml?faces-redirect=true";
		} else {
			navResult = "departamentoLista.xhtml?faces-redirect=true";
		}
		return navResult;
	}
}
