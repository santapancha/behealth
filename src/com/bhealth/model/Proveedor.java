package com.bhealth.model;

public class Proveedor {
	Integer id;
	String rfc;
	String razonSocial;
	String razonFiscal;
	String domicilio;
	String correo;
	String telefono;
	
	public Proveedor() {
		super();
	}
	
	public Proveedor(Integer id, String rfc, String razonSocial, String razonFiscal, String domicilio, String correo, String telefono) {
		super();
		this.id = id;
		this.rfc = rfc;
		this.razonSocial = razonSocial;
		this.razonFiscal = razonFiscal;
		this.domicilio = domicilio;
		this.correo = correo;
		this.telefono = telefono;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getRfc() {
		return rfc;
	}
	
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	
	public String getRazonSocial() {
		return razonSocial;
	}
	
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	
	public String getDomicilio() {
		return domicilio;
	}
	
	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}
	
	public String getCorreo() {
		return correo;
	}
	
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public String getTelefono() {
		return telefono;
	}
	
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
