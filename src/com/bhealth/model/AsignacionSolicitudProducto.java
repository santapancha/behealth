package com.bhealth.model;

public class AsignacionSolicitudProducto {
	SolicitudCompra solicitudCompra;
	Producto producto;
	
	public AsignacionSolicitudProducto() {
		super();
 	}
		
	public AsignacionSolicitudProducto(SolicitudCompra solicitudCompra, Producto producto) {
		super();
		this.solicitudCompra = solicitudCompra;
		this.producto = producto;
	}
	
	public SolicitudCompra getSolicitudCompra() {
		return solicitudCompra;
	}
	
	public void setSolicitudCompra(SolicitudCompra solicitudcompra) {
		this.solicitudCompra = solicitudcompra;
	}
	
	public Producto getProducto() {
		return producto;
	}
	
	public void setProducto(Producto producto) {
		this.producto = producto;
		
	}

}
