package com.bhealth.model;

public class AsignacionDepartamentoFamilia {
	Departamento departamento;
	Familia familia;
	
	AsignacionDepartamentoFamilia() {
		super();
	}
	
	AsignacionDepartamentoFamilia(Departamento departamento, Familia familia) {
		super();
		this.departamento = departamento;
		this.familia = familia;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}
}
