package com.bhealth.model;

public class Compra {
	Integer folio;
	SolicitudCompra solicitudCompra;
	StatusCompra statusCompra;
	Usuario usuario;
	
	public Compra() {
		super();
	}
	
	public Compra(Integer folio, SolicitudCompra solicitudCompra, StatusCompra statusCompra, Usuario usuario) {
		super();
		this.folio = folio;
		this.solicitudCompra = solicitudCompra;
		this.statusCompra = statusCompra;
		this.usuario = usuario;
	}
	
	public Integer getFolio() {
		return folio;
	}
	
	public void setFolio(Integer folio) {
		this.folio = folio;
	}
	
	public SolicitudCompra getSolicitudCompra() {
		return solicitudCompra;
	}
	
	public void setSolicitudCompra(SolicitudCompra solicitudCompra) {
		this.solicitudCompra = solicitudCompra;
	}
	
	public StatusCompra getStatusCompra() {
		return statusCompra;
	}
	
	public void setStatusCompra(StatusCompra statusCompra) {
		this.statusCompra = statusCompra;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
