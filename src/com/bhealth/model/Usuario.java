package com.bhealth.model;

public class Usuario {
	
	String usuario;
	String tipo;
	String nombre;
	String correo;
	String contrasena;
	
	public Usuario() {
		super();
	}
	
	public Usuario(String usuario, String tipo, String nombre, String correo, String contrasena) {
		super();
		this.usuario = usuario;
		this.tipo = tipo;
		this.nombre = nombre;
		this.correo = correo;
		this.contrasena = contrasena;
	}
	
	public String getUsuario() {
		return usuario;
		
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	public String getTipo() {
		return tipo;
		
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getNombre() {
		return nombre;
		
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getCorreo() {
		return correo;
		
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	public String getContrasena() {
		return contrasena;
		
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}

}
