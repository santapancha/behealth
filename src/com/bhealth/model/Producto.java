package com.bhealth.model;

public class Producto {
	Integer id;
	String nombre;
	Familia familia;
	Unidad unidad;
	Proveedor proveedor;
	
	Producto() {
		super();
	}
	
	Producto(Integer id, String nombre, Familia familia, Unidad unidad, Proveedor proveedor) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.familia = familia;
		this.unidad = unidad;
		this.proveedor = proveedor;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Familia getFamilia() {
		return familia;
	}
	
	public void setFamilia(Familia familia) {
		this.familia = familia;
	}
	
	public Unidad getUnidad() {
		return unidad;
	}
	
	public void setUnidad(Unidad unidad) {
		this.unidad = unidad;
	}
	
	public Proveedor getProveedor() {
		return proveedor;
	}
	
	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}
}
