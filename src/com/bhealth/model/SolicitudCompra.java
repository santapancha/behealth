package com.bhealth.model;

import java.util.Date;

public class SolicitudCompra {
	Integer folio;
	Date fecha;
	String descripcion;
	Familia familia;
	Usuario usuario;

	public SolicitudCompra() {
		super();
	}

	public SolicitudCompra(Integer folio, Date fecha, String descripcion, Familia familia, Usuario usuario) {
		super();
		this.folio = folio;
		this.fecha = fecha;
		this.descripcion = descripcion;
		this.familia = familia;
		this.usuario = usuario;
	}

	public Integer getFolio() {
		return folio;
	}

	public void setFolio(Integer folio) {
		this.folio = folio;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
